# ESP32 Setup and Interfacing instructions
### Introduction
ESP32 is a system on a chip that integrates the following features:

    Wi-Fi (2.4 GHz band)
    Bluetooth 4.2
    Dual high performance cores
    Ultra Low Power co-processor
    Several peripherals

### What You Need

Hardware:

    An ESP32 board
    USB cable - USB A / micro USB B
    Computer running Windows, Linux, or macOS

Software:

    Toolchain to compile code for ESP32
    Build tools - CMake and Ninja to build a full Application for ESP32
    ESP-IDF that essentially contains API (software libraries and source code) for ESP32 and scripts to operate the Toolchain
    Text editor to write programs (Projects) in C

## Installation Step by Step

This is a detailed roadmap to walk you through the installation process.
Setting up Development Environment

    Step 1. Install prerequisites for Windows, Linux or macOS
    Step 2. Get ESP-IDF
    Step 3. Set up the tools
    Step 4. Set up the environment variables

#### Step 1. Install Prerequisites
To compile with ESP-IDF you need to get the following packages:

Ubuntu [and Debian] (My setup):

    sudo apt-get install git wget flex bison gperf python python-pip python-setuptools python-serial python-click python-cryptography python-future python-pyparsing python-pyelftools cmake ninja-build ccache

CentOS 7:

    sudo yum install git wget flex bison gperf python pyserial python-pyelftools cmake ninja-build ccache

Arch:

    sudo pacman -S --needed gcc git make flex bison gperf python2-pip python2-pyserial python2-click python2-cryptography python2-future python2-pyparsing python2-pyelftools cmake ninja ccache

#### Step 2. Get ESP-IDF
To build applications for the ESP32, you need the software libraries provided by Espressif in [ESP-IDF repository](https://github.com/espressif/esp-idf).

To get ESP-IDF, navigate to your installation directory and clone the repository with git clone

    cd ~/esp
    git clone --recursive https://github.com/espressif/esp-idf.git

ESP-IDF will be downloaded into ~/esp/esp-idf

#### Step 3. Setup tools

    cd ~/esp/esp-idf
    ./install.sh

#### Step 4. Setup environment variables

    export IDF_PATH=~/esp/esp-idf
    export PATH="$IDF_PATH/tools:$PATH"
    export PATH="$PATH:$HOME/esp/xtensa-esp32-elf/bin"
    alias get_esp32='export PATH="$PATH:$HOME/esp/xtensa-esp32-elf/bin"'
    sudo chmod -R 777 /dev/ttyUSB0
    . $HOME/esp/esp-idf/export.sh

Add these path variables to your profile or <b> execute every time you restart your PC </b>

## Sample programs
### Hello World
    
    cd ~/esp
    cp -r $IDF_PATH/examples/get-started/hello_world .

    cd ~/esp/hello_world
    idf.py menuconfig

If the previous steps have been done correctly, a menu appears:
To navigate and use menuconfig, press the following keys:

    Arrow keys for navigation

    Enter to go into a submenu

    Esc to go up one level or exit

    ? to see a help screen. Enter key exits the help screen

    Space, or Y and N keys to enable (Yes) and disable (No) configuration items with checkboxes “[*]”

    ? while highlighting a configuration item to display help about that item

    / to find configuration items

Build the project by running:

    idf.py build

This command will compile the application and all ESP-IDF components, then it will generate the bootloader, partition table, and application binaries.

    $ idf.py build
    Running cmake in directory /path/to/hello_world/build
    Executing "cmake -G Ninja --warn-uninitialized /path/to/hello_world"...
    Warn about uninitialized values.
    -- Found Git: /usr/bin/git (found version "2.17.0")
    -- Building empty aws_iot component due to configuration
    -- Component names: ...
    -- Component paths: ...

    ... (more lines of build system output)

    [527/527] Generating hello-world.bin
    esptool.py v2.3.1

    Project build complete. To flash, run this command:
    ../../../components/esptool_py/esptool/esptool.py -p (PORT) -b 921600 write_flash --flash_mode dio --flash_size detect --flash_freq 40m 0x10000 build/hello-world.bin  build 0x1000 build/bootloader/bootloader.bin 0x8000 build/partition_table/partition-table.bin
    or run 'idf.py -p PORT flash'

Flash the binaries that you just built onto your ESP32 board by running:

    # idf.py -p PORT [-b BAUD] flash
    idf.py -p /dev/ttyUSB0 flash

To check if “hello_world” is indeed running, type idf.py -p PORT monitor (Do not forget to replace PORT with your serial port name).

This command launches the IDF Monitor application:

    $ idf.py -p /dev/ttyUSB0 monitor
    Running idf_monitor in directory [...]/esp/hello_world/build
    Executing "python [...]/esp-idf/tools/idf_monitor.py -b 115200 [...]/esp/hello_world/build/hello-world.elf"...
    --- idf_monitor on /dev/ttyUSB0 115200 ---
    --- Quit: Ctrl+] | Menu: Ctrl+T | Help: Ctrl+T followed by Ctrl+H ---
    ets Jun  8 2016 00:22:57

    rst:0x1 (POWERON_RESET),boot:0x13 (SPI_FAST_FLASH_BOOT)
    ets Jun  8 2016 00:22:57
    ...

After startup and diagnostic logs scroll up, you should see “Hello world!” printed out by the application.

    ...
    Hello world!
    Restarting in 10 seconds...
    I (211) cpu_start: Starting scheduler on APP CPU.
    Restarting in 9 seconds...
    Restarting in 8 seconds...
    Restarting in 7 seconds...

To exit IDF monitor use the shortcut <b>Ctrl + ]</b>  .

##### You can combine building, flashing and monitoring into one step by running:

    idf.py -p PORT flash monitor

## WPA2 Enterprise Example

This example shows how ESP32 connects to AP with wpa2 enterprise encryption. Example does the following steps:

1. Install CA certificate which is optional.
2. Install client certificate and client key which is required in TLS method and optional in PEAP and TTLS methods.
3. Set identity of phase 1 which is optional.
4. Set user name and password of phase 2 which is required in PEAP and TTLS methods.
5. Enable wpa2 enterprise.
6. Connect to AP.

*Note:* 
1. certificate currently is generated when compiling the example and then stored in flash.
2. The expiration date of the certificates is 2027/06/05.

The file wpa2_ca.pem, wpa2_ca.key, wpa2_server.pem, wpa2_server.crt and wpa2_server.key can be used to configure AP with wpa2 enterprise encryption. The steps how to generate new certificates and keys using openssl is as follows:
   
1. wpa2_ca.pem wpa2_ca.key:
    openssl req -new -x509 -keyout wpa2_ca.key -out wpa2_ca.pem
2. wpa2_server.key:
    openssl req -new -key wpa2_server.key -out wpa2_server.csr
3. wpa2_csr:
    openssl req -new -key server.key -out server.csr
4. wpa2_server.crt:
    openssl ca -batch -keyfile wpa2_ca.key -cert wpa2_ca.pem -in wpa2_server.csr -key ca1234 -out wpa2_server.crt -extensions xpserver_ext -extfile xpextensions
5. wpa2_server.p12:
    openssl pkcs12 -export -in wpa2_server.crt -inkey wpa2_server.key -out wpa2_server.p12 -passin pass:sv1234 -passout pass:sv1234
6. wpa2_server.pem:
    openssl pkcs12 -in wpa2_server.p12 -out wpa2_server.pem -passin pass:sv1234 -passout pass:sv1234
7. wpa2_client.key:
    openssl genrsa -out wpa2_client.key 1024
8. wpa2_client.csr:
    openssl req -new -key wpa2_client.key -out wpa2_client.csr
9. wpa2_client.crt:
    openssl ca -batch -keyfile wpa2_ca.key -cert wpa2_ca.pem -in wpa2_client.csr -key ca1234 -out wpa2_client.crt -extensions xpclient_ext -extfile xpextensions
10. wpa2_client.p12:
    openssl pkcs12 -export -in wpa2_client.crt -inkey wpa2_client.key -out wpa2_client.p12
11. wpa2_client.pem:
    openssl pkcs12 -in wpa2_client.p12 -out wpa2_client.pem

### Example output

Here is an example of wpa2 enterprise(PEAP method) console output.

    I (1352) example: Setting WiFi configuration SSID wpa2_test...
    I (1362) wpa: WPA2 ENTERPRISE VERSION: [v2.0] enable

    I (1362) wifi: rx_ba=1 tx_ba=1

    I (1372) wifi: mode : sta (24:0a:c4:03:b8:dc)
    I (3002) wifi: n:11 0, o:1 0, ap:255 255, sta:11 0, prof:11
    I (3642) wifi: state: init -> auth (b0)
    I (3642) wifi: state: auth -> assoc (0)
    I (3652) wifi: state: assoc -> run (10)
    I (3652) wpa: wpa2_task prio:24, stack:6144

    I (3972) wpa: >>>>>wpa2 FINISH

    I (3982) wpa: wpa2 task delete

    I (3992) wifi: connected with wpa2_test, channel 11
    I (5372) example: ~~~~~~~~~~~
    I (5372) example: IP:0.0.0.0
    I (5372) example: MASK:0.0.0.0
    I (5372) example: GW:0.0.0.0
    I (5372) example: ~~~~~~~~~~~
    I (6832) event: ip: 192.168.1.112, mask: 255.255.255.0, gw: 192.168.1.1
    I (7372) example: ~~~~~~~~~~~
    I (7372) example: IP:192.168.1.112
    I (7372) example: MASK:255.255.255.0
    I (7372) example: GW:192.168.1.1
    I (7372) example: ~~~~~~~~~~~
    I (9372) example: ~~~~~~~~~~~
    I (9372) example: IP:192.168.1.112
    I (9372) example: MASK:255.255.255.0
    I (9372) example: GW:192.168.1.1
    I (9372) example: ~~~~~~~~~~~

#### Execution :
go to ~/esp/esp-idf/examples/wifi/wpa2_enterprise/

    cd ~/esp/esp-idf/examples/wifi/wpa2_enterprise/

Execute the following:

    idf.py menuconfig
    idf.py build
    idf.py -p /dev/ttyUSB0 flash monitor
*<b>boot the device | reset the device | see the console output</b>*

## mqtt TCP Implementation

    cd esp-idf/examples/protocols/mqtt/tcp/

#### code
    #include <stdio.h>
    #include <stdint.h>
    #include <stddef.h>
    #include <string.h>
    #include "esp_wifi.h"
    #include "esp_system.h"
    #include "nvs_flash.h"
    #include "esp_event.h"
    #include "esp_netif.h"
    #include "protocol_examples_common.h"

    #include "freertos/FreeRTOS.h"
    #include "freertos/task.h"
    #include "freertos/semphr.h"
    #include "freertos/queue.h"

    #include "lwip/sockets.h"
    #include "lwip/dns.h"
    #include "lwip/netdb.h"

    #include "esp_log.h"
    #include "mqtt_client.h"

    static const char *TAG = "MQTT_EXAMPLE";


    static esp_err_t mqtt_event_handler_cb(esp_mqtt_event_handle_t event)
    {
        esp_mqtt_client_handle_t client = event->client;
        int msg_id;
        // your_context_t *context = event->context;
        switch (event->event_id) {
            case MQTT_EVENT_CONNECTED:
                ESP_LOGI(TAG, "MQTT_EVENT_CONNECTED");
                msg_id = esp_mqtt_client_publish(client, "/topic/qos1", "data_3", 0, 1, 0);
                ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);

                msg_id = esp_mqtt_client_subscribe(client, "/topic/qos0", 0);
                ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

                msg_id = esp_mqtt_client_subscribe(client, "/topic/qos1", 1);
                ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

                msg_id = esp_mqtt_client_unsubscribe(client, "/topic/qos1");
                ESP_LOGI(TAG, "sent unsubscribe successful, msg_id=%d", msg_id);
                break;
            case MQTT_EVENT_DISCONNECTED:
                ESP_LOGI(TAG, "MQTT_EVENT_DISCONNECTED");
                break;

            case MQTT_EVENT_SUBSCRIBED:
                ESP_LOGI(TAG, "MQTT_EVENT_SUBSCRIBED, msg_id=%d", event->msg_id);
                msg_id = esp_mqtt_client_publish(client, "/topic/qos0", "data", 0, 0, 0);
                ESP_LOGI(TAG, "sent publish successful, msg_id=%d", msg_id);
                break;
            case MQTT_EVENT_UNSUBSCRIBED:
                ESP_LOGI(TAG, "MQTT_EVENT_UNSUBSCRIBED, msg_id=%d", event->msg_id);
                break;
            case MQTT_EVENT_PUBLISHED:
                ESP_LOGI(TAG, "MQTT_EVENT_PUBLISHED, msg_id=%d", event->msg_id);
                break;
            case MQTT_EVENT_DATA:
                ESP_LOGI(TAG, "MQTT_EVENT_DATA");
                printf("TOPIC=%.*s\r\n", event->topic_len, event->topic);
                printf("DATA=%.*s\r\n", event->data_len, event->data);
                break;
            case MQTT_EVENT_ERROR:
                ESP_LOGI(TAG, "MQTT_EVENT_ERROR");
                break;
            default:
                ESP_LOGI(TAG, "Other event id:%d", event->event_id);
                break;
        }
        return ESP_OK;
    }

    static void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data) {
        ESP_LOGD(TAG, "Event dispatched from event loop base=%s, event_id=%d", base, event_id);
        mqtt_event_handler_cb(event_data);
    }

    static void mqtt_app_start(void)
    {
        esp_mqtt_client_config_t mqtt_cfg = {
            .uri = CONFIG_BROKER_URL,
        };
    #if CONFIG_BROKER_URL_FROM_STDIN
        char line[128];

        if (strcmp(mqtt_cfg.uri, "FROM_STDIN") == 0) {
            int count = 0;
            printf("Please enter url of mqtt broker\n");
            while (count < 128) {
                int c = fgetc(stdin);
                if (c == '\n') {
                    line[count] = '\0';
                    break;
                } else if (c > 0 && c < 127) {
                    line[count] = c;
                    ++count;
                }
                vTaskDelay(10 / portTICK_PERIOD_MS);
            }
            mqtt_cfg.uri = line;
            printf("Broker url: %s\n", line);
        } else {
            ESP_LOGE(TAG, "Configuration mismatch: wrong broker url");
            abort();
        }
    #endif /* CONFIG_BROKER_URL_FROM_STDIN */

        esp_mqtt_client_handle_t client = esp_mqtt_client_init(&mqtt_cfg);
        esp_mqtt_client_register_event(client, ESP_EVENT_ANY_ID, mqtt_event_handler, client);
        esp_mqtt_client_start(client);
    }

    void app_main(void)
    {
        ESP_LOGI(TAG, "[APP] Startup..");
        ESP_LOGI(TAG, "[APP] Free memory: %d bytes", esp_get_free_heap_size());
        ESP_LOGI(TAG, "[APP] IDF version: %s", esp_get_idf_version());

        esp_log_level_set("*", ESP_LOG_INFO);
        esp_log_level_set("MQTT_CLIENT", ESP_LOG_VERBOSE);
        esp_log_level_set("MQTT_EXAMPLE", ESP_LOG_VERBOSE);
        esp_log_level_set("TRANSPORT_TCP", ESP_LOG_VERBOSE);
        esp_log_level_set("TRANSPORT_SSL", ESP_LOG_VERBOSE);
        esp_log_level_set("TRANSPORT", ESP_LOG_VERBOSE);
        esp_log_level_set("OUTBOX", ESP_LOG_VERBOSE);

        ESP_ERROR_CHECK(nvs_flash_init());
        esp_netif_init();
        ESP_ERROR_CHECK(esp_event_loop_create_default());

        /* This helper function configures Wi-Fi or Ethernet, as selected in menuconfig.
        * Read "Establishing Wi-Fi or Ethernet Connection" section in
        * examples/protocols/README.md for more information about this function.
        */
        ESP_ERROR_CHECK(example_connect());

        mqtt_app_start();
    }

#### change the following in the above code:
    
    msg_id = esp_mqtt_client_subscribe(client, "/topic/[qos0]", 0);
    ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);

    msg_id = esp_mqtt_client_subscribe(client, "/topic/[qos1]", 1);
    ESP_LOGI(TAG, "sent subscribe successful, msg_id=%d", msg_id);


* the values [qos0] and [qos1] should be changed to the correct topic values.


        esp_mqtt_client_config_t mqtt_cfg = {
        .uri = CONFIG_BROKER_URL,
* the *CONFIG_BROKER_URL* has to be replaced with the correct string. (follow your server guidelines to add username and password, if required).

#### Run as expected.
